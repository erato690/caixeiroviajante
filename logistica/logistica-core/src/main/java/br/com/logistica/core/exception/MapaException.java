package br.com.logistica.core.exception;

public class MapaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1761538175431413095L;

	public MapaException() {
		super();
	}

	public MapaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MapaException(String message, Throwable cause) {
		super(message, cause);
	}

	public MapaException(String message) {
		super(message);
	}

	public MapaException(Throwable cause) {
		super(cause);
	}
	
	
	

}
