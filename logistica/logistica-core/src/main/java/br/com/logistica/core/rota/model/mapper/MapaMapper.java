package br.com.logistica.core.rota.model.mapper;

import br.com.logistica.core.rota.model.Mapa;
import br.com.logistica.dao.entity.MapaEntity;

public class MapaMapper {

	public static MapaEntity paraEntidade(Mapa mapa) {

		MapaEntity mapaEntity = new MapaEntity();
		mapaEntity.setNome(mapa.getNome());

		return mapaEntity;

	}
}
