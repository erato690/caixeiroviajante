package br.com.logistica.core.exception;

public class GerarRotaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8442840482391333627L;

	public GerarRotaException() {
		super();
	}

	public GerarRotaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GerarRotaException(String message, Throwable cause) {
		super(message, cause);
	}

	public GerarRotaException(String message) {
		super(message);
	}

	public GerarRotaException(Throwable cause) {
		super(cause);
	}
	
	
	

}
