package br.com.logistica.core.rota.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Rota implements Comparable<Rota> {
	
	@NotNull
	@Size(min=1)
	private String pontoOrigem;
	
	@NotNull
	@Size(min=1)
	private String pontoDestino;
	
	private double distancia;
	
	public Rota(){
	}
	
	public Rota(String origem , String destino , int distancia){
		
		this.pontoOrigem = origem;
		this.pontoDestino = destino;
		this.distancia = distancia;	
	}

	public String getPontoOrigem() {
		return pontoOrigem;
	}

	public void setPontoOrigem(String pontoOrigem) {
		this.pontoOrigem = pontoOrigem;
	}

	public String getPontoDestino() {
		return pontoDestino;
	}

	public void setPontoDestino(String pontoDestino) {
		this.pontoDestino = pontoDestino;
	}



	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pontoDestino == null) ? 0 : pontoDestino.hashCode());
		result = prime * result + ((pontoOrigem == null) ? 0 : pontoOrigem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rota other = (Rota) obj;
		if (pontoDestino == null) {
			if (other.pontoDestino != null)
				return false;
		} else if (!pontoDestino.equals(other.pontoDestino))
			return false;
		if (pontoOrigem == null) {
			if (other.pontoOrigem != null)
				return false;
		} else if (!pontoOrigem.equals(other.pontoOrigem))
			return false;
		return true;
	}

	@Override
	public int compareTo(Rota rota) {
		
		if(rota.equals(this))
			return 0;
		return 1;
	}


	
	


	
}

