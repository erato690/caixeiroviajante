package br.com.logistica.core.rota.model;

import java.io.Serializable;

public class RotaEscolhida implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nomeMapa;
	
	private TrajetoRota  caminho;
	
	private double custo;

	public String getNomeMapa() {
		return nomeMapa;
	}

	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}

	public double getCusto() {
		return custo;
	}

	public void setCusto(double custo) {
		this.custo = custo;
	}

	public TrajetoRota getCaminho() {
		return caminho;
	}

	public void setCaminho(TrajetoRota caminho) {
		this.caminho = caminho;
	}


	
	
	
	

	
	
	
	
}
