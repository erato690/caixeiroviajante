package br.com.logistica.core.rota.pesquisa;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import org.springframework.stereotype.Component;

import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.core.rota.model.TrajetoRota;

@Component
public class PesquisaTodasrotasExistentes implements PesquisaRotas {

	public List<TrajetoRota> pesquisarPossivelRotas(List<Rota> rotasInicial, List<Rota> todasRotas,
			Rota rotaPesquisa) {

		List<TrajetoRota> listaTrajetos = new ArrayList<TrajetoRota>();
		StringBuilder label = new StringBuilder();

		for (Rota rotaInicial : rotasInicial) {
			
			Rota rotaTemp = null;
			Queue<Rota> subRotas = new PriorityQueue<Rota>();		
			double totalRota = 0;
			boolean trajetoCompleto = false;			
			subRotas.add(rotaInicial);

			while (!trajetoCompleto) {

				rotaTemp = rotaInicial;				
				label.append(rotaInicial.getPontoOrigem());
				label.append(rotaInicial.getPontoDestino());

				for (Rota rotaCaminho : todasRotas) {

					if (rotaTemp.getPontoDestino().equals(rotaCaminho.getPontoOrigem())) {
										
						totalRota += rotaTemp.getDistancia();				
						label.append(rotaCaminho.getPontoOrigem());
						label.append(rotaCaminho.getPontoDestino());				
						subRotas.add(rotaCaminho);
						rotaTemp = rotaCaminho;

					}else if (rotaTemp.getPontoOrigem().equals(rotaCaminho.getPontoOrigem())){
						
						totalRota += rotaTemp.getDistancia();
						label.append(rotaCaminho.getPontoOrigem());
						label.append(rotaCaminho.getPontoDestino());	
						subRotas.add(rotaCaminho);
						rotaTemp = rotaCaminho;					
					} 
					
					if (rotaTemp.getPontoDestino().equals(rotaPesquisa.getPontoDestino())) {

						trajetoCompleto = true;				
						TrajetoRota trajeto = new TrajetoRota();
						trajeto.setRota(subRotas);
						trajeto.setLabelRota(label.toString());
						trajeto.setQuantidadeKMTotal(totalRota);	
						label= new StringBuilder();					
						listaTrajetos.add(trajeto);					
						break;
					}

				}
				
				if(totalRota == 0){
					
					break;
				}

			}

		}

		return listaTrajetos;
	}
	

}
