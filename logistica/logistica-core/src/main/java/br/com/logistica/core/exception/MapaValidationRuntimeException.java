package br.com.logistica.core.exception;

public class MapaValidationRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4681580734938796811L;

	public MapaValidationRuntimeException() {
		super();
	}

	public MapaValidationRuntimeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MapaValidationRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MapaValidationRuntimeException(String message) {
		super(message);
	}

	public MapaValidationRuntimeException(Throwable cause) {
		super(cause);
	}
	
	
	

}
