package br.com.logistica.core.rota.gerador;

import br.com.logistica.core.exception.GerarRotaException;
import br.com.logistica.core.rota.model.RotaEscolhida;
import br.com.logistica.core.rota.model.TrajetoVeiculo;

public interface GeradorRotas {

	public abstract RotaEscolhida gerarRota(TrajetoVeiculo trajeto) throws GerarRotaException; 
}
