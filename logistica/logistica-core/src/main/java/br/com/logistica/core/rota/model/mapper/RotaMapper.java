package br.com.logistica.core.rota.model.mapper;

import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.dao.entity.RotaEntity;

public class RotaMapper {

	public static RotaEntity paraEntidade(Rota rota) {

		RotaEntity rotaEntity = new RotaEntity();
		rotaEntity.setDistancia(rota.getDistancia());
		rotaEntity.setPontoDestino(rota.getPontoDestino());
		rotaEntity.setPontoOrigem(rota.getPontoOrigem());

		return rotaEntity;

	}

	public static Rota deEntidade(RotaEntity rotaEntity) {

		Rota rota = new Rota();
		rota.setDistancia(rotaEntity.getDistancia());
		rota.setPontoDestino(rotaEntity.getPontoDestino());
		rota.setPontoOrigem(rotaEntity.getPontoOrigem());

		return rota;

	}
}
