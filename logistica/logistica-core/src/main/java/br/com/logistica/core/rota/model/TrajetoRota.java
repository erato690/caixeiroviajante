package br.com.logistica.core.rota.model;

import java.io.Serializable;
import java.util.Queue;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class TrajetoRota implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3342416920785965596L;

	private String nomeMapa;
	
	private double quantidadeKMTotal;
	
	@Size(min=1,message="É necessario uma lisa com mais de um elemento")
	@NotNull(message="É necessario uma litsa valida para calcular as rotas")
	private Queue<Rota> rota;
	
	
	private String labelRota;

	public String getNomeMapa() {
		return nomeMapa;
	}

	public void setQuantidadeKMTotal(int quantidadeKMTotal) {
		this.quantidadeKMTotal = quantidadeKMTotal;
	}

	public String getLabelRota() {
		return labelRota;
	}

	public void setLabelRota(String labelRota) {
		this.labelRota = labelRota;
	}

	public Queue<Rota> getRota() {
		return rota;
	}

	public void setRota(Queue<Rota> rota) {
		this.rota = rota;
	}

	public double getQuantidadeKMTotal() {
		return quantidadeKMTotal;
	}

	public void setQuantidadeKMTotal(double quantidadeKMTotal) {
		this.quantidadeKMTotal = quantidadeKMTotal;
	}

	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
