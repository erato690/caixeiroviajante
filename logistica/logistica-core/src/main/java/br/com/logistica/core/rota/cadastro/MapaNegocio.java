package br.com.logistica.core.rota.cadastro;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.logistica.core.exception.MapaException;
import br.com.logistica.core.exception.MapaValidationRuntimeException;
import br.com.logistica.core.rota.model.Mapa;
import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.core.rota.model.mapper.MapaMapper;
import br.com.logistica.core.rota.model.mapper.RotaMapper;
import br.com.logistica.core.rota.validacao.MensagemTratamento;
import br.com.logistica.dao.entity.MapaEntity;
import br.com.logistica.dao.entity.RotaEntity;
import br.com.logistica.dao.repository.MapaRepository;
import br.com.logistica.dao.repository.RotaRepository;

@Component
public class MapaNegocio implements IMapaNegocio {

	
	@Autowired
	private MapaRepository mapaRepository;
	
	@Autowired
	private RotaRepository rotaRepository;
	
	@Autowired
	private MensagemTratamento mensagemTratamento;


	@Override
	@Transactional
	public void cadastraMapa(Mapa mapa) throws MapaException  {
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();	
		Set<ConstraintViolation<Mapa>> constraintViolations = validator.validate(mapa);
		
		if(!constraintViolations.isEmpty()){	
			String erro = this.mensagemTratamento.tratar(constraintViolations);
			throw new MapaValidationRuntimeException(erro);
		}
		
		MapaEntity mapaEntity = MapaMapper.paraEntidade(mapa);
		
		 List<RotaEntity> listaRotas = new ArrayList<RotaEntity> ();
		
		if(mapa.getRotas() != null && !mapa.getRotas().isEmpty()){
			
			mapa.getRotas().forEach(rota -> {
				
				if(rota != null){
					
					listaRotas.add(RotaMapper.paraEntidade(rota));
				}
			});
			
		}
	
		try{
			this.mapaRepository.save(mapaEntity);
		}catch (Exception e) {
			throw new MapaException("Erro ao salvar mapa no banco");
		}
		
	}


	@Override
	public void cadastraRota(String nomeMapa, Rota rota) throws MapaException {
		
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();	
		Set<ConstraintViolation<Rota>> constraintViolations = validator.validate(rota);
		
		if(!constraintViolations.isEmpty()){	
			String erro = this.mensagemTratamento.tratar(constraintViolations);
			throw new MapaValidationRuntimeException(erro);
		}
		
		RotaEntity rotaEntity = RotaMapper.paraEntidade(rota);
		
	
		try{
			MapaEntity mapaEntity = this.mapaRepository.obterMapa(nomeMapa);
			rotaEntity.setMapa(mapaEntity);
			this.rotaRepository.save(rotaEntity);
		}catch (Exception e) {
			throw new MapaException("Erro ao salvar rota no banco");
		}
		
	}


	public MapaRepository getMapaRepository() {
		return mapaRepository;
	}


	public void setMapaRepository(MapaRepository mapaRepository) {
		this.mapaRepository = mapaRepository;
	}


	public RotaRepository getRotaRepository() {
		return rotaRepository;
	}


	public void setRotaRepository(RotaRepository rotaRepository) {
		this.rotaRepository = rotaRepository;
	}


	public MensagemTratamento getMensagemTratamento() {
		return mensagemTratamento;
	}


	public void setMensagemTratamento(MensagemTratamento mensagemTratamento) {
		this.mensagemTratamento = mensagemTratamento;
	}
	
	
	
	
	
	
	


}
