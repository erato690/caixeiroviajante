package br.com.logistica.core.rota.cadastro;

import br.com.logistica.core.exception.MapaException;
import br.com.logistica.core.rota.model.Mapa;
import br.com.logistica.core.rota.model.Rota;

public interface IMapaNegocio{

	public void cadastraMapa(Mapa mapa) throws MapaException;
	
	public void cadastraRota(String nomeMapa, Rota rota) throws MapaException;
}
