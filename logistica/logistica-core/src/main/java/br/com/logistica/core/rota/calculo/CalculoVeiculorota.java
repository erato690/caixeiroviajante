package br.com.logistica.core.rota.calculo;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.logistica.core.exception.MapaValidationRuntimeException;
import br.com.logistica.core.rota.model.TrajetoRota;
import br.com.logistica.core.rota.model.TrajetoVeiculo;
import br.com.logistica.core.rota.validacao.MensagemTratamento;

/**
 * Classe responsavel para o calcular o valor da gasolina.
 * Esse valor é gerado pela quantidade total  de KM do 
 * trajeto dividido pelo consumo do automovel vezes o custo da gasolina.
 *  
 * 
 * @author Rafael
 *
 */
@Component
public class CalculoVeiculorota implements CalculoRota {
	
	@Autowired
	private MensagemTratamento mensagemTratamento;

	@Override
	public Double calculoTrajeto(TrajetoRota trajetoRota,TrajetoVeiculo trajetoVeiculo) {
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();	
		Set<ConstraintViolation<TrajetoRota>> constraintViolations = validator.validate(trajetoRota);
		
		if(!constraintViolations.isEmpty()){	
			String erro = this.mensagemTratamento.tratar(constraintViolations);
			throw new MapaValidationRuntimeException(erro);
		}
		
		double quantidadeLitrosconsumido = trajetoRota.getQuantidadeKMTotal() / trajetoVeiculo.getAutonomiaAutomovel();  
		
		BigDecimal resultado = new BigDecimal(quantidadeLitrosconsumido * trajetoVeiculo.getCustoGasolina());
		
		resultado.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		return resultado.doubleValue();
		
		
	}
	
}
