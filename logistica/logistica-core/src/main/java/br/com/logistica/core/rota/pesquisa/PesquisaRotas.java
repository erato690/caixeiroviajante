package br.com.logistica.core.rota.pesquisa;

import java.util.List;

import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.core.rota.model.TrajetoRota;


public interface PesquisaRotas {

	
	public List<TrajetoRota> pesquisarPossivelRotas(List<Rota> rotasInicial,
			List<Rota> rotasCompletas, Rota rotaPesquisa);
		
}
