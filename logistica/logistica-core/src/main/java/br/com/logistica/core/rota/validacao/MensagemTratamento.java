package br.com.logistica.core.rota.validacao;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.stereotype.Component;

@Component
public class MensagemTratamento {


	public <T> String tratar(Set<ConstraintViolation<T>> constraintViolations ){
		final StringBuilder erroMensagem = new StringBuilder();
		
		 constraintViolations.iterator().forEachRemaining(erro ->{
			 erroMensagem.append(erro.getMessage());
		 });
		 
		 return erroMensagem.toString();
	}
	

}
