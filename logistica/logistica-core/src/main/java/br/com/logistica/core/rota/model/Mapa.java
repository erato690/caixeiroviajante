package br.com.logistica.core.rota.model;

import java.util.List;

import javax.validation.constraints.NotNull;

public class Mapa {

	@NotNull(message="nome do mapa não pode ser vazio")
	private String nome;
	
	private List<Rota> rotas;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Rota> getRotas() {
		return rotas;
	}
	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}
	
	
}
