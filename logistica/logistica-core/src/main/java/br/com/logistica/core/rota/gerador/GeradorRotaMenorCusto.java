package br.com.logistica.core.rota.gerador;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.logistica.core.exception.GerarRotaException;
import br.com.logistica.core.rota.calculo.CalculoRota;
import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.core.rota.model.RotaEscolhida;
import br.com.logistica.core.rota.model.TrajetoRota;
import br.com.logistica.core.rota.model.TrajetoVeiculo;
import br.com.logistica.core.rota.model.mapper.RotaMapper;
import br.com.logistica.core.rota.pesquisa.PesquisaRotas;
import br.com.logistica.core.rota.validacao.MensagemTratamento;
import br.com.logistica.dao.entity.RotaEntity;
import br.com.logistica.dao.repository.RotaRepository;

/**
 * 
 * CLASSE RESPONSAVEL POR GERAR UMA ROTA COM O MENOR CUSTO.
 * 
 * @author Rafael
 *
 */
@Component
public class GeradorRotaMenorCusto implements GeradorRotas {


	@Autowired
	private PesquisaRotas pesquisaTodasRotasexistente;

	@Autowired
	private RotaRepository rotaRepository;

	@Autowired
	private CalculoRota calculoRota;
	
	@Autowired
	private MensagemTratamento mensagemTratamento;

	@Transactional
	public RotaEscolhida gerarRota(TrajetoVeiculo veiculo) throws GerarRotaException {
		
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();	
		Set<ConstraintViolation<TrajetoVeiculo>> constraintViolations = validator.validate(veiculo);
		
		if(!constraintViolations.isEmpty()){	
			String erro = this.mensagemTratamento.tratar(constraintViolations);
			throw new GerarRotaException(erro);
		}
		

		 TrajetoRota trajetoEncontrado= null;
		 RotaEscolhida rotaEscolhida = new RotaEscolhida();

		

			List<RotaEntity> rotasBanco = rotaRepository.obterRotas(veiculo.getNomeMapa());

			if (rotasBanco != null && !rotasBanco.isEmpty()) {

				List<RotaEntity> rotaIniciais = rotasBanco.stream()
						.filter(rota -> rota.getPontoOrigem().equals(veiculo.getRota().getPontoOrigem()))
						.collect(Collectors.toList());

				rotasBanco = rotasBanco.stream()
						.filter(rota -> !(rota.getPontoOrigem().equals(veiculo.getRota().getPontoOrigem())))
						.collect(Collectors.toList());

				List<Rota> listaRotacompontoInicial = new ArrayList<Rota>();
				List<Rota> listaRotaBancosempontoInicial = new ArrayList<Rota>();

				rotaIniciais.forEach(rotaEntity -> {
					if (rotaEntity != null)
						listaRotacompontoInicial.add(RotaMapper.deEntidade(rotaEntity));
				});

				rotasBanco.forEach(rotaEntity -> {
					if (rotaEntity != null)
						listaRotaBancosempontoInicial.add(RotaMapper.deEntidade(rotaEntity));
				});

				List<TrajetoRota> todosTrajetosexistente = this.pesquisaTodasRotasexistente.pesquisarPossivelRotas(
						listaRotacompontoInicial, listaRotaBancosempontoInicial, veiculo.getRota());
				
				double custoMenorrota = Double.MAX_VALUE;
				
				for(TrajetoRota trajeto : todosTrajetosexistente )
				{		
					
					trajeto.setNomeMapa(veiculo.getNomeMapa());
					if( this.calculoRota.calculoTrajeto(trajeto, veiculo) < custoMenorrota   )
						trajetoEncontrado = trajeto; 			
				}
				
				
				rotaEscolhida.setCaminho(trajetoEncontrado);
				rotaEscolhida.setCusto(custoMenorrota);
				rotaEscolhida.setNomeMapa(veiculo.getNomeMapa());

				
			} else {

				throw new GerarRotaException("Nenhuma rota entrada para o trajeto");
			}
	

			return rotaEscolhida;
	}
}
