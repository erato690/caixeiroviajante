package br.com.logistica.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.com.logistica.dao.config.DaoConfig;

@Configuration
@ComponentScan(basePackages="br.com.logistica.*")
@Import(value={DaoConfig.class})
public class CoreConfig {

}
