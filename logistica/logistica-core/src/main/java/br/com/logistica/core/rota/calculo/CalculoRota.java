package br.com.logistica.core.rota.calculo;

import br.com.logistica.core.rota.model.TrajetoRota;
import br.com.logistica.core.rota.model.TrajetoVeiculo;
/**
 * Interface responsavel por realizar 
 * o calculo do trajeto do veiculo.
 * 
 * @author Rafael
 *
 */
public interface CalculoRota {

	
	public Double calculoTrajeto(TrajetoRota trajetosRotas,TrajetoVeiculo trajetoVeiculo);
}
