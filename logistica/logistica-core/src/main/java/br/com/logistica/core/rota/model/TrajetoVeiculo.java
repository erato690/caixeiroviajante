package br.com.logistica.core.rota.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TrajetoVeiculo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7828707948702760279L;

	
	@NotNull(message="É necesserario preencher  o nome do mapa")
	private String nomeMapa;
	
	@NotNull(message="É necesserario preencher a rota")
	private Rota  rota;
	
	@NotNull(message="É necessario informar  a autonimoa do veiculo")
	private Integer autonomiaAutomovel;

	@NotNull(message="É necessario informar o custo da gasolina")
	private Double custoGasolina;

	public Integer getAutonomiaAutomovel() {
		return autonomiaAutomovel;
	}

	public void setAutonomiaAutomovel(Integer autonomiaAutomovel) {
		this.autonomiaAutomovel = autonomiaAutomovel;
	}


	public Double getCustoGasolina() {
		return custoGasolina;
	}

	public void setCustoGasolina(Double custoGasolina) {
		this.custoGasolina = custoGasolina;
	}

	public String getNomeMapa() {
		return nomeMapa;
	}

	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}
	
	
	
	
	
}
