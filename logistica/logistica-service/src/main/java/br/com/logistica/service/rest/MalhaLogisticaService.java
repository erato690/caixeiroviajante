package br.com.logistica.service.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.logistica.core.exception.GerarRotaException;
import br.com.logistica.core.exception.MapaException;
import br.com.logistica.core.exception.MapaValidationRuntimeException;
import br.com.logistica.core.rota.cadastro.IMapaNegocio;
import br.com.logistica.core.rota.gerador.GeradorRotas;
import br.com.logistica.core.rota.model.Mapa;
import br.com.logistica.core.rota.model.Rota;
import br.com.logistica.core.rota.model.TrajetoVeiculo;
import br.com.logistica.service.model.ResponseTrajetoVeiculo;


@Controller
public class MalhaLogisticaService {

	@Autowired
	private IMapaNegocio cadastroMapa;

	@Autowired
	private GeradorRotas geradorRotas;

	@RequestMapping(method = RequestMethod.POST,value="/cadastroMapa",consumes="application/json",produces="application/json" )
	public  @ResponseBody String cadastroMapa(@RequestBody  Mapa mapa) {

		String mensagem = "{mensagem: ";

		try {

			this.cadastroMapa.cadastraMapa(mapa);
			
			mensagem += "Cadastro efetuado";

		} catch (MapaValidationRuntimeException ex) {
			mensagem += ex.getMessage();
		} catch (MapaException ex) {
			mensagem += ex.getMessage();
		} catch (Exception ex) {
			mensagem += ex.getMessage();
		}

		return mensagem + "}";
	}
	
	@RequestMapping(method = RequestMethod.POST,value="/{mapa}/cadastroRota",consumes="application/json",produces="application/json" )
	public  @ResponseBody String cadastroRota(@RequestBody  Rota rota,@PathVariable("mapa")String nomeMapa) {

		String mensagem = "{mensagem: ";

		try {

			this.cadastroMapa.cadastraRota(nomeMapa, rota);
			
			mensagem += "Cadastro efetuado";

		} catch (MapaValidationRuntimeException ex) {
			mensagem += ex.getMessage();
		} catch (MapaException ex) {
			mensagem += ex.getMessage();
		} catch (Exception ex) {
			mensagem += ex.getMessage();
		}

		return mensagem + "}";
	}


	@RequestMapping(method = RequestMethod.POST,value="/obterRota",consumes="application/json",produces="application/json" )
	public @ResponseBody ResponseTrajetoVeiculo obterMelhorRota(@RequestBody TrajetoVeiculo trajeto) {

		ResponseTrajetoVeiculo retorno = new ResponseTrajetoVeiculo();

		try {
			
			retorno.setTrajeto(geradorRotas.gerarRota(trajeto));
		} catch (MapaValidationRuntimeException ex) {
			retorno.setErro(ex.getMessage());
		} catch (GerarRotaException ex) {
			retorno.setErro(ex.getMessage());
		} catch (Exception ex) {
			retorno.setErro(ex.getMessage());
		}

		return retorno;
	}
	
}
