package br.com.logistica.config;

import org.apache.catalina.servlets.WebdavServlet;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.com.logistica.core.config.CoreConfig;

@Configuration
@ComponentScan(basePackages = { "br.com.logistica.*" })
@Import(value = { CoreConfig.class })
public class Config {

	@Bean
	ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebdavServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}
}
