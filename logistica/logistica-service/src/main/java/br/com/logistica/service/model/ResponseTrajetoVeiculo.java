package br.com.logistica.service.model;

import java.io.Serializable;

import br.com.logistica.core.rota.model.RotaEscolhida;



public class ResponseTrajetoVeiculo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1450010359258999952L;
	
	

	private RotaEscolhida trajeto;
	
	private String erro;


	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public RotaEscolhida getTrajeto() {
		return trajeto;
	}

	public void setTrajeto(RotaEscolhida trajeto) {
		this.trajeto = trajeto;
	}
	
	
	
	
}
