package br.com.logistica.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "br.com.logistica.*")
@Import({ Config.class})
@EnableAutoConfiguration
public class Server   {
	

	public static void main(String[] args) {
		
		SpringApplication.run(Server.class, args);
	}


}
