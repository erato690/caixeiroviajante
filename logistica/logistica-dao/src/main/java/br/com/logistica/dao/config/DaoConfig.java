package br.com.logistica.dao.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages="br.com.logistica.*")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.logistica.dao.repository", entityManagerFactoryRef = "mysqlEntityManager", transactionManagerRef = "transactionManager")
@PropertySource(value = { "classpath:application-local.yml" })
public class DaoConfig {

	
	public static final String PACOTE_SCAN ="br.com.logistica.dao.entity";
	
	@Autowired
	private ConnectionProperties connectionProperties;
	
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean(name="transactionManager")
	public PlatformTransactionManager transactionManager() {
		
		EntityManagerFactory factory = entityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}
	

	@Bean(name="mysqlEntityManager")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setShowSql(true);

		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(PACOTE_SCAN);
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.dialect", connectionProperties.getHibernateDialect());
		jpaProperties.put("hibernate.hbm2ddl.auto","create");
		//jpaProperties.put("hibernate.c3p0.max_size", connectionProperties.getPollMaxSize());
		//jpaProperties.put("hibernate.c3p0.timeout", connectionProperties.getPollTimeout());
		//jpaProperties.put("hibernate.c3p0.max_statements", connectionProperties.getPollMaxStattement());
		//jpaProperties.put("hibernate.c3p0.idle_test_period", this.profile.getHibernateTag().getPollConection().getIdle_test_period());

		factory.setJpaProperties(jpaProperties);
		factory.afterPropertiesSet();
		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		return factory;
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	@Bean
	public DataSource dataSource() {
		
		BasicDataSource dataSource = new BasicDataSource();
		
		dataSource.setDriverClassName(this.connectionProperties.getDriverClass());
		dataSource.setUrl(this.connectionProperties.getUrl());
		dataSource.setUsername(this.connectionProperties.getUsuario());
		dataSource.setPassword(this.connectionProperties.getSenha());
		dataSource.setDefaultAutoCommit(true);

		return dataSource;
	}
	

	

}
