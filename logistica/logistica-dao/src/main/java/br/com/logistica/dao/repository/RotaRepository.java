package br.com.logistica.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.logistica.dao.entity.RotaEntity;



@Repository
public interface RotaRepository extends JpaRepository<RotaEntity, Long>  {

	@Query("from RotaEntity where "
			  + "mapa.nome = :nomeMapa ")
	public List<RotaEntity> obterRotas(@Param("nomeMapa")String nomeMapa);
}
