package br.com.logistica.dao.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class MapaEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(unique=true)
	private String nome;
	
	@OneToMany(fetch=FetchType.LAZY,cascade={CascadeType.ALL})
	private List<RotaEntity> rotas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RotaEntity> getRotas() {
		return rotas;
	}

	public void setRotas(List<RotaEntity> rotas) {
		this.rotas = rotas;
	}
	
	


	
	
	
}
