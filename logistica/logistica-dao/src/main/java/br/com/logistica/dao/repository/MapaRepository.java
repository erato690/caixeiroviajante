package br.com.logistica.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.logistica.dao.entity.MapaEntity;

public interface MapaRepository extends JpaRepository<MapaEntity, Long>  {

	
	@Query("from MapaEntity where "
			  + "nome = :nomeMapa ")
	public MapaEntity obterMapa(@Param("nomeMapa")String nomeMapa);
}
