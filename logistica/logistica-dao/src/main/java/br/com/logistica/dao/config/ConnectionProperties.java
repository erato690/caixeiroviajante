package br.com.logistica.dao.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class ConnectionProperties {

	@Value("${url}")
	private String url;
	
	@Value("${driverClass}")
	private String driverClass;
	
	@Value("${usuario}")
	private String usuario;
	
	@Value("${senha}")
	private String senha;
	
	@Value("${hibernateDialect}")
	private String hibernateDialect;
	
	@Value("${pollMinSize}")
	private int pollMinSize;
	
	@Value("${pollMaxSize}")
	private int pollMaxSize;
	
	@Value("${pollTimeout}")
	private int pollTimeout;
	
	@Value("${pollMaxStattement}")
	private int pollMaxStattement;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getHibernateDialect() {
		return hibernateDialect;
	}

	public void setHibernateDialect(String hibernateDialect) {
		this.hibernateDialect = hibernateDialect;
	}

	public int getPollMinSize() {
		return pollMinSize;
	}

	public void setPollMinSize(int pollMinSize) {
		this.pollMinSize = pollMinSize;
	}

	public int getPollMaxSize() {
		return pollMaxSize;
	}

	public void setPollMaxSize(int pollMaxSize) {
		this.pollMaxSize = pollMaxSize;
	}

	public int getPollTimeout() {
		return pollTimeout;
	}

	public void setPollTimeout(int pollTimeout) {
		this.pollTimeout = pollTimeout;
	}

	public int getPollMaxStattement() {
		return pollMaxStattement;
	}

	public void setPollMaxStattement(int pollMaxStattement) {
		this.pollMaxStattement = pollMaxStattement;
	}
	
	
	
}
