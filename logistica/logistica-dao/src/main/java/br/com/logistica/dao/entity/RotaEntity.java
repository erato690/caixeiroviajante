package br.com.logistica.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class RotaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="pontoOrigem",nullable=false)
	private String pontoOrigem;
	
	@Column(name="pontoDestino",nullable=false)
	private String pontoDestino;
	
	@Column(name="distancia",nullable=false)
	private double distancia;
	
	@ManyToOne(optional=false,fetch=FetchType.LAZY)
	private MapaEntity mapa;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPontoOrigem() {
		return pontoOrigem;
	}

	public void setPontoOrigem(String pontoOrigem) {
		this.pontoOrigem = pontoOrigem;
	}

	public String getPontoDestino() {
		return pontoDestino;
	}

	public void setPontoDestino(String pontoDestino) {
		this.pontoDestino = pontoDestino;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public MapaEntity getMapa() {
		return mapa;
	}

	public void setMapa(MapaEntity mapa) {
		this.mapa = mapa;
	}




	
}
